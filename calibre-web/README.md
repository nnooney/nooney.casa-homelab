# Calibre Web

Calibre Web provides a web application interface to manage an e-book library.

## Operating the stack

```shell
# Start the stack
docker-compose config | docker -H ssh://docker.nooney.casa stack deploy -c - calibre-web

# Stop the stack
docker -H ssh://docker.nooney.casa stack rm calibre-web
```
