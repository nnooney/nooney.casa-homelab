# Jellyfin

Jellyfin provides a media system for my home lab.

## Operating the stack

```shell
# Start the stack
docker-compose config | docker -H ssh://docker.nooney.casa stack deploy -c - jellyfin

# Stop the stack
docker -H ssh://docker.nooney.casa stack rm jellyfin
```
