# Devbox

The devbox provides a fully-capable remote development environment for all my
coding needs.

## Operating the stack

```shell
# Start the stack
docker-compose config | docker -H ssh://docker.nooney.casa stack deploy -c - devbox

# Stop the stack
docker -H ssh://docker.nooney.casa stack rm devbox
```
