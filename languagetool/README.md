# Languagetool

Languagetool provides grammar and spellchecking for a variety of languages.

## Adding n-gram data

N-gram data is available for download on the
[Languagetool Website](http://languagetool.org/download/ngram-data/). Download
it to the host machine and configure the path via the `NGRAMS_HOST_PATH` in
`.env`. For example, if the n-gram English data is stored at
`/mnt/data/ngram/en`, then `NGRAMS_HOST_PATH` should be set to
`/mnt/data/ngram`.

## Operating the stack

```shell
# Start the stack
docker-compose config | docker -H ssh://docker.nooney.casa stack deploy -c - languagetool

# Stop the stack
docker -H ssh://docker.nooney.casa stack rm languagetool
```
