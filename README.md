# `nooney.casa` Home lab

This repository contains configuration for applications run in my home lab.

My home lab consists of several nodes running Docker in Swarm mode.

## Organization

### Applications

Each folder in this project contains one application. The application is
described by a `docker-compose.yml` file, and is meant to be managed via the
Docker Swarm CLI.

```shell
# Start the stack
docker -H ssh://docker.nooney.casa stack deploy -c <docker-compose.yml> <stack_name>

# Stop the stack
docker -H ssh://docker.nooney.casa stack rm <stack_name>
```

The value provided for `docker-compose.yml` can be preprocessed using
`docker-compose`. This enables `.env` substitution.

```shell
# Another way to start the stack
docker-compose config | docker -H ssh://docker.nooney.casa stack deploy -c - <stack_name>
```
