# Traefik

Traefik provides reverse proxy services for all other docker containers.

## Operating the stack

```shell
# Start the stack
docker-compose config | docker -H ssh://docker.nooney.casa stack deploy -c - traefik

# Stop the stack
docker -H ssh://docker.nooney.casa stack rm traefik
```

## Managing the `traefik-public` network

In order to connect other applications to Traefik, a separate network called
`traefik-public` is established. It is managed separately to decouple the
docker network from the Traefik container.

```shell
# Create the traefik-public network
docker -H ssh://docker.nooney.casa network create --driver=overlay traefik-public
```

## `config`

Traefik uses locally provided SSL certificates, configured via
`CERTIFICATE_NAME` and `CERTIFICATE_PATH`. The files in the `config/` directory
must be manually copied over to the `traefik` named volume path on the docker
host so the Traefik container can find the configuration.
